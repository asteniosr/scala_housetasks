package features.step_definitions

import cucumber.api.scala.ScalaDsl
import cucumber.api.scala.EN
import cucumber.runtime.PendingException

class AHousetasklist extends ScalaDsl with EN{
	Given("""^Set a database$"""){ () =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
	When("""^I go to the housework list page$"""){ () =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
	Then("""^I should see "([^"]*)"$"""){ (arg0:String) =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
	When("""^I fill in "([^"]*)" with "([^"]*)"$"""){ (arg0:String, arg1:String) =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
	When("""^I push "([^"]*)"$"""){ (arg0:String) =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
	Then("""^I am in the home page$"""){ () =>
	  //// Express the Regexp above with the code you wish you had
	  throw new PendingException()
	}
}