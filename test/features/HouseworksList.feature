Feature: HouseTaskList
  
  In order to manage my houseworks easily, and then I could do all my houseworks and accumulate points for win a prize.
  As a couple's member
  I want to manage housetasks on the web pages.

  Scenario: Puts a task
  	Given Set a database   
    When I go to the housetasks list page
    Then I should see "0 task"
    When I fill in "Housework" with "wash the dishes"
    And  I push "create"
    Then I should see "1 task"
    And I am in the housetasks list page
    And  I should see "wash the dishes"